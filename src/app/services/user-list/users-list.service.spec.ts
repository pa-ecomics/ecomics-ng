/*
 * Project : ecomics-ng
 * FileName : users-list.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { UsersListService } from './users-list.service';

describe('UsersListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersListService = TestBed.get(UsersListService);
    expect(service).toBeTruthy();
  });
});
