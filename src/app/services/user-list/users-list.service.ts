/*
 * Project : ecomics-ng
 * FileName : users-list.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {UserHttpService} from "../http/user/user-http.service";
import {Role, User} from "../../models";

@Injectable({
    providedIn: 'root'
})
export class UsersListService {
    errorMessage: string;
    successMessage: string;
    users: User[];

    constructor(private userHttp: UserHttpService) {
        this.userHttp.getAllUser().subscribe(users => {
            this.users = users;
        });
    }

    removeRole(user: User, role: Role) {
        if (window.confirm('Remove this role ?')) {
            this.userHttp.removeRole(user.id, role.id).subscribe((response) => {
                if (response.succeed) {
                    user.removeRole(role);
                }
            });
        }
    }

    removeRoleForAllUser(role: Role) {
        this.users.forEach(template => {
            template.removeRole(role);
        })
    }

    deleteUser(user: User) {
        if (window.confirm('Delete this user ?')) {
            this.userHttp.deleteUser(user.id).subscribe((response) => {
                if (response.succeed) {
                    this.successMessage = 'User deleted !';
                    this.errorMessage = null;

                    this.users.splice(this.users.indexOf(user), 1);
                } else {
                    this.successMessage = null;
                    this.errorMessage = response.message;
                }
            });

            setTimeout(() => {
                this.successMessage = null;
                this.errorMessage = null;
            }, 4000);
        }
    }
}
