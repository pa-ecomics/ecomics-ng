/*
 * Project : ecomics-ng
 * FileName : router.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class RouterService {
    constructor(private Router: Router) {
    }

    getCurrentUrl(): string {
        return this.Router.url
    }

    navigate(url: string): void {
        this.Router.navigate([url]);
    }
}
