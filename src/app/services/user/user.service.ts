/*
 * Project : ecomics-ng
 * FileName : user.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {SessionService} from "..";
import {UserHttpService} from "../http";
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from "angularx-social-login";
import {User} from "../../models";
import {HeaderService} from "../header/header.service";
import {RouterService} from "../router/router.service";
import {Url} from "../../enums";


@Injectable({
    providedIn: 'root'
})
export class UserService {
    email: string;
    password: string;
    confirmPassword: string;
    firstName: string;
    lastName: string;
    error: string;
    errorPassword: string;
    successData: string;
    successPassword: string;

    constructor(private userHttp: UserHttpService, private authService: AuthService, private sessionService: SessionService, private routerService: RouterService, private headerService: HeaderService) {
    }

    resetMessage(): void {
        this.error = null;
        this.successData = null;
        this.successPassword = null;
    }

    loginPassword(): void {
        this.userHttp.loginPassword(this.email, this.password).subscribe((response: { succeed: boolean, data, message?: string, token?: string }) => {
            if (!response.succeed) {
                this.error = response.message;
            } else {
                this.finalLogin(response.data);
            }
        });
    }

    register(): void {
        this.userHttp.register(this.email, this.password, this.firstName, this.lastName).subscribe((response: { succeed: boolean, data, message?: string, token?: string }) => {
            if (!response.succeed) {
                this.error = response.message;
            } else {
                this.finalLogin(response.data);
            }
        });
    }

    checkPassword(forPassword: boolean): void {
        let message = this.password && this.confirmPassword && this.password !== this.confirmPassword ? 'Both password should be the same' : null;

        if (forPassword) {
            this.errorPassword = message;
        } else {
            this.error = message;
        }
    }

    registerButtonIsDisabled(): boolean {
        return this.password !== this.confirmPassword || !this.email;
    }

    updatePasswordButtonIsDisabled(): boolean {
        return !this.password || !this.confirmPassword || this.password !== this.confirmPassword;
    }

    updateButtonIsDisabled(): boolean {
        let user = this.sessionService.getUser();

        return this.email === user.email && this.firstName === user.firstName && this.lastName === user.lastName;
    }

    updateData() {
        let user = this.sessionService.getUser();

        this.userHttp.updateUserData(user.id, this.email, this.firstName, this.lastName).subscribe((response: { succeed: boolean, data: { id: number, email: string, firstName: string, lastName: string}, message?: string, token?: string }) => {
            if (!response.succeed) {
                this.error = response.message;
                this.successData = null;
            } else {
                this.error = null;
                this.successData = 'User updated';
                this.headerService.user = this.sessionService.getUser();
                setTimeout(() => {this.successData = null}, 4000);
            }
        })
    }

    updatePassword() {
        let user = this.sessionService.getUser();

        this.userHttp.updateUserPassword(user.id, this.password).subscribe((response: { succeed: boolean, data: { id: number, email: string, firstName: string, lastName: string}, message?: string, token?: string }) => {
            if (!response.succeed) {
                this.errorPassword = response.message;
                this.successPassword = null;
            } else {
                this.errorPassword = null;
                this.successPassword = 'Password updated';
                this.password = null;
                this.confirmPassword = null;
                setTimeout(() => {this.successPassword = null}, 4000);
            }
        })
    }

    socialSignIn(socialPlatform: string): void {
        let socialPlatformProvider;
        if (socialPlatform == "facebook") {
            socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        } else if (socialPlatform == "google") {
            socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }

        this.authService.signIn(socialPlatformProvider).then(
            (userData) => {
                this.loginSocial(userData.email, socialPlatform, userData.id, userData.firstName, userData.lastName);
            }
        );
    }

    initUser(): void {
        this.resetMessage();

        let user = this.sessionService.getUser();

        this.email = user.email;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
    }

    deleteUser() {
        if (window.confirm('Delete your account ?')) {
            let user = this.sessionService.getUser();

            this.userHttp.deleteUser(user.id).subscribe((response) => {
                if (response.succeed) {
                    this.routerService.navigate(Url.login);
                } else {
                    this.successData = null;
                    this.error = response.message;
                }
            });
        }
    }

    private loginSocial(email: string, socialName: string, socialId: string, firstName: string, lastName: string): void {
        this.userHttp.loginSocial(email, socialName, socialId, firstName, lastName).subscribe((response: { succeed: boolean, data: { id: number, email: string, firstName: string, lastName: string }, message?: string, token?: string }) => {
            if (!response.succeed) {
                this.error = response.message;
            } else {
                this.finalLogin(response.data);
            }
        })
    }

    private finalLogin(userData): void {
        let user = User.parse(userData);
        this.sessionService.setUser(user);

        this.routerService.navigate(Url.home);
    }
}
