/*
 * Project : ecomics-ng
 * FileName : user.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import {HttpClientModule} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService, AuthServiceConfig} from "angularx-social-login";
import {provideConfig} from "../../modules";

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule, RouterTestingModule],
    providers: [AuthService, {provide: AuthServiceConfig, useFactory: provideConfig}],
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });
});
