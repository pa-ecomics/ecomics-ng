/*
 * Project : ecomics-ng
 * FileName : filter-select.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {Filter} from "../../models";
import {TemplateHttpService} from "../http";

@Injectable({
    providedIn: 'root'
})
export class FilterSelectService {
    filters: Filter[];

    constructor(private templateHttp: TemplateHttpService) {
        this.templateHttp.getAllFilter().subscribe(filters => {
            this.filters = filters;
        });
    }

    removeFilter(filter: Filter) {
        this.filters.splice(this.filters.indexOf(filter), 1);
    }
}
