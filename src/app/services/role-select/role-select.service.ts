/*
 * Project : ecomics-ng
 * FileName : role-select.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {Role, Tag} from "../../models";
import {UserHttpService} from "../http";

@Injectable({
    providedIn: 'root'
})
export class RoleSelectService {
    roles: Role[];

    constructor(private httpUser: UserHttpService) {
        this.httpUser.getAllRole().subscribe(roles => {
            this.roles = roles;
        });
    }

    removeRole(role: Role) {
        this.roles.splice(this.roles.indexOf(role), 1);
    }
}
