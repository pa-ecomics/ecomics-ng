/*
 * Project : ecomics-ng
 * FileName : role-select.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { RoleSelectService } from './role-select.service';

describe('RoleSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoleSelectService = TestBed.get(RoleSelectService);
    expect(service).toBeTruthy();
  });
});
