/*
 * Project : ecomics-ng
 * FileName : edit-bd.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { EditBdService } from './edit-bd.service';

describe('EditBdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditBdService = TestBed.get(EditBdService);
    expect(service).toBeTruthy();
  });
});
