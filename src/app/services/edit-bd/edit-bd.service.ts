/*
 * Project : ecomics-ng
 * FileName : edit-bd.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import Page from "../../models/Page";
import Bd from "../../models/Bd";

@Injectable({
    providedIn: 'root'
})
export class EditBdService {
    bd: Bd;
    currentPage: Page;

    constructor() {
    }

    initView(bd: Bd) {
        this.bd = bd;
        this.currentPage = this.bd.pages[0];
    }

    previewPage() {
        let currentIndex = this.bd.pages.indexOf(this.currentPage);
        let previewIndex = currentIndex === 0 ? this.bd.pages.length - 1 : currentIndex - 1;

        this.currentPage = this.bd.pages[previewIndex];
    }

    nextPage() {
        let currentIndex = this.bd.pages.indexOf(this.currentPage);
        let nextIndex = currentIndex < this.bd.pages.length - 1 ? currentIndex + 1 : 0;

        this.currentPage = this.bd.pages[nextIndex];
    }
}
