/*
 * Project : ecomics-ng
 * FileName : session.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { SessionService } from './session.service';
import {RouterTestingModule} from "@angular/router/testing";

describe('SessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule],
  }));

  it('should be created', () => {
    const service: SessionService = TestBed.get(SessionService);
    expect(service).toBeTruthy();
  });
});
