/*
 * Project : ecomics-ng
 * FileName : session.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {User} from "../../models";
import {Url} from "../../enums";
import {CryptoService} from "../crypto/crypto.service";
import {RouterService} from "../router/router.service";

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    private token: string;
    private user: User;

    constructor(private routerService: RouterService, private CryptoService: CryptoService) {}

    checkSession() {
        this.token = this.getStorage('token') || null;

        this.user = User.parse(this.getStorage('user')) || null;

        if (!this.token || !this.user) {
            this.routerService.navigate(Url.login);
        }
    }

    setUser(user: User): void {
        if (user) {
            this.user = user;
            this.setStorage('user', user.toString());
        }
    }

    setToken(token: string): void {
        if (token) {
            this.token=token;
            this.setStorage('token', token);
        }
    }

    getUser(): User {
        let user = this.user || User.parse(this.getStorage('user')) || null;

        if (!user) {
            this.routerService.navigate(Url.login);
        }

        return user;
    }

    getToken(): String {
        let token = this.token || this.getStorage('token') || null;

        if (!token) {
            this.routerService.navigate(Url.login);
        }

        return token;
    }

    closeSession() {
        this.token = null;
        this.user = null;
        localStorage.clear();


        if (![Url.login.toString(), Url.register.toString()].includes(this.routerService.getCurrentUrl())) {
            this.routerService.navigate(Url.login);
        }
    }

    private setStorage = (key: string, data): void => {
        let encryptData = this.CryptoService.encrypt(data);

        localStorage.setItem(key, encryptData);
    };

    private getStorage = (key: string): string => {
        let encryptData = localStorage.getItem(key);

        return this.CryptoService.decrypt(encryptData);
    };
}
