/*
 * Project : ecomics-ng
 * FileName : template-model.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {Filter, Template} from "../../models";
import {TemplateHttpService} from "../http";
import {TemplatePageService} from "../template-page/template-page.service";

@Injectable({
    providedIn: 'root'
})
export class TemplateModelService {
    errorMessage: string;
    successMessage: string;
    title: string;
    description: string;
    numberPage: number;
    miniature: File = null;
    oldTemplate: Template;
    imageUrl: any;
    loading: boolean = false;
    filter: Filter | 'none';

    constructor(private templateHttp: TemplateHttpService, private templatePageService: TemplatePageService) {
    }

    initUpdateTemplate(template: Template) {
        this.oldTemplate = template;
        this.title = template.title;
        this.description = template.description;
        this.imageUrl = template.imageUrl;
        this.loading = false;
        this.filter = template.filter || 'none';
        this.imageUrl = template.imageUrl;
    }

    initNewTemplate() {
        this.oldTemplate = null;
        this.title = null;
        this.description = null;
        this.numberPage = 1;
        this.loading = false;
        this.filter = 'none';
    }

    handleFileInput(target: any) {
        this.miniature = target.files[0];

        let reader = new FileReader();
        reader.readAsDataURL(this.miniature);
        reader.onload = (_event) => {
            this.imageUrl = reader.result;
        }
    }

    createTemplate() {
        if (this.numberPage < 1) {
            this.errorMessage = 'Template must have page !';
            this.successMessage = null;
        } else if (this.description.length > 200) {
            this.errorMessage = 'Template description less than 200 characters !';
            this.successMessage = null;
        } else {
            this.loading = true;
            let filterId = this.filter !== 'none' ? this.filter.id : null;

            this.templateHttp.createTemplate(this.title, this.description, this.miniature, this.numberPage, filterId).subscribe((response) => {
                if (response.succeed) {
                    /*print message*/
                    this.errorMessage = null;
                    this.successMessage = 'Template created !';

                    /*Add template for template list page*/
                    let template = Template.parse(response.data);
                    if (!this.templatePageService.templates) {
                        this.templatePageService.templates = [];
                    }
                    this.templatePageService.templates.push(template);

                    /* clear form */
                    this.title = null;
                    this.numberPage = 0;
                    this.description = null;
                    this.miniature = null;
                    this.imageUrl = null;
                    this.filter = null;
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                this.loading = false;
            })
        }

        setTimeout(() => {
            this.successMessage = null;
            this.errorMessage = null;
        }, 4000);
    }

    updateTemplate() {
        if (this.description.length > 200) {
            this.errorMessage = 'Template description less than 200 characters !';
            this.successMessage = null;
        } else {
            let filterId = this.filter && this.filter !== 'none' ? this.filter.id : null;

            this.templateHttp.updateTemplate(this.oldTemplate.id, this.title, this.description, this.miniature, filterId).subscribe((response) => {
                if (response.succeed) {
                    /*print message*/
                    this.errorMessage = null;
                    this.successMessage = 'Template updated !';

                    /*Add template for template list page*/
                    let template = Template.parse(response.data);
                    this.oldTemplate.imageUrl = template.reloadImage();
                    this.oldTemplate.filter = this.filter;
                    this.templatePageService.updateTemplateById(template.id, template, this.filter);
                    this.filter = template.filter || 'none';
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }
            });
        }

        setTimeout(() => {
            this.errorMessage = null;
            this.successMessage = null;
        }, 4000);
    }

    isDisabledNewForm(): boolean {
        return !this.title || !this.description || this.numberPage === null || !this.filter || !this.miniature;
    }

    isDisabledUpdateForm(): boolean {
        return this.title === this.oldTemplate.title && this.description === this.oldTemplate.description && this.oldTemplate.imageUrl === this.imageUrl && this.filter === this.oldTemplate.filter;
    }
}
