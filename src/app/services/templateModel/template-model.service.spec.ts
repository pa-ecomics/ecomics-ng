/*
 * Project : ecomics-ng
 * FileName : template-model.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { TemplateModelService } from './template-model.service';

describe('TemplateModelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplateModelService = TestBed.get(TemplateModelService);
    expect(service).toBeTruthy();
  });
});
