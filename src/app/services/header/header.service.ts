/*
 * Project : ecomics-ng
 * FileName : header.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {User} from "../../models";
import {Url} from "../../enums";

@Injectable({
    providedIn: 'root'
})
export class HeaderService {
    user: User;
    url = Url;
    currentUrl: Url;

    constructor() {}
}
