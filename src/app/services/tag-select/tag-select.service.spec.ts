/*
 * Project : ecomics-ng
 * FileName : tag-select.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { TagSelectService } from './tag-select.service';

describe('TagSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TagSelectService = TestBed.get(TagSelectService);
    expect(service).toBeTruthy();
  });
});
