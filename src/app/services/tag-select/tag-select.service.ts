/*
 * Project : ecomics-ng
 * FileName : tag-select.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {Tag} from "../../models";
import {TemplateHttpService} from "../http";

@Injectable({
    providedIn: 'root'
})
export class TagSelectService {
    tags: Tag[];

    constructor(private httpTemplate: TemplateHttpService) {
        this.httpTemplate.getAllTag().subscribe(tags => {
            this.tags = tags;
        });
    }

    removeTag(tag: Tag) {
        this.tags.splice(this.tags.indexOf(tag), 1);
    }
}
