/*
 * Project : ecomics-ng
 * FileName : handle-token-http.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {SessionService} from "../session/session.service";
import {User} from "../../models";
import {environment} from "../../../environments/environment";
import {Url} from "../../enums";
import {RouterService} from "../router/router.service";

@Injectable({
    providedIn: 'root'
})
export class HandleTokenHttpService {
    constructor(private routerService: RouterService, private sessionService: SessionService) {
    }

    handleRequestResponse(data, saveUser: boolean = false) {
        if (!data.succeed && environment.tokenErrorMessage.includes(data.message)) {
            this.routerService.navigate(Url.login);
        }

        if (data.token) {
            this.sessionService.setToken(data.token);
        }

        if (saveUser && data.succeed) {
            let user = User.parse(data.data);
            this.sessionService.setUser(user);
        }
    }
}
