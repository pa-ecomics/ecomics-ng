/*
 * Project : ecomics-ng
 * FileName : bd-http.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { BdHttpService } from './bd-http.service';

describe('BdHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BdHttpService = TestBed.get(BdHttpService);
    expect(service).toBeTruthy();
  });
});
