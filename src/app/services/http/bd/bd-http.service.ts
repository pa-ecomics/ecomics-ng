/*
 * Project : ecomics-ng
 * FileName : bd-http.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "../..";
import {HandleTokenHttpService} from "../handle-token-http.service";
import Bd from "../../../models/Bd";

@Injectable({
    providedIn: 'root'
})
export class BdHttpService {
    baseUrl = environment.apiUrl;

    constructor(private http: HttpClient, private globalHttpService: HandleTokenHttpService, private sessionService: SessionService) {
    }

    getCurrentUserBd(): any {
        let userId = this.sessionService.getUser().id;
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/bd/mines?userId=${userId}&token=${token}`, {}).pipe(
            map((response: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(response);

                let bds: Bd[] = [];
                if (response.succeed) {
                    response.data.forEach(bdData => {
                        bds.push(Bd.parse(bdData));
                    });
                }

                return bds;
            })
        );
    }

    createBd(templateId: number): any {
        return this.http.post(`${this.baseUrl}/bd`, {
            userId: this.sessionService.getUser().id,
            templateId: templateId,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    deleteBd(bdId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/bd?bdId=${bdId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    downloadBd(bdId: number): any {
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/bd/download?bdId=${bdId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }
}
