/*
 * Project : ecomics-ng
 * FileName : page-http.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HandleTokenHttpService} from "../handle-token-http.service";
import {SessionService} from "../..";
import {environment} from "../../../../environments/environment";
import {map} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class PageHttpService {
    baseUrl = environment.apiUrl;

    constructor(private http: HttpClient, private globalHttpService: HandleTokenHttpService, private sessionService: SessionService) {
    }

    removeFrameImage(frameId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/frame/image?frameId=${frameId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    updateTemplateFrame(frameId: number, indication: string, frameImage: File): any {
        const formData: FormData = new FormData();

        let data = {
            frameId: frameId,
            indication: indication,
            token: this.sessionService.getToken()
        };

        formData.append('formData', JSON.stringify(data));
        if (frameImage) {
            formData.append('frameImage', frameImage, frameImage.name);
        }

        return this.http.put(`${this.baseUrl}/frame`, formData).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    updateBdFrame(bdId: number, frameId: number, frameImage: File): any {
        const formData: FormData = new FormData();

        let data = {
            bdId: bdId,
            frameId: frameId,
            token: this.sessionService.getToken()
        };

        formData.append('formData', JSON.stringify(data));
        if (frameImage) {
            formData.append('frameImage', frameImage, frameImage.name);
        }

        return this.http.post(`${this.baseUrl}/bd/fillOneFrame`, formData).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    removeBdFrame(bdId: number, frameId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/bd/fillOneFrame?bdId=${bdId}&frameId=${frameId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }
}
