/*
 * Project : ecomics-ng
 * FileName : page-http.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { PageHttpService } from './page-http.service';

describe('PageHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PageHttpService = TestBed.get(PageHttpService);
    expect(service).toBeTruthy();
  });
});
