/*
 * Project : ecomics-ng
 * FileName : handle-token-http.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { HandleTokenHttpService } from './handle-token-http.service';

describe('HandleTokenHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HandleTokenHttpService = TestBed.get(HandleTokenHttpService);
    expect(service).toBeTruthy();
  });
});
