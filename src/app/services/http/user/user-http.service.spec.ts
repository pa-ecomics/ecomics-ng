/*
 * Project : ecomics-ng
 * FileName : user-http.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {TestBed} from '@angular/core/testing';

import {UserHttpService} from './user-http.service';
import {HttpClientModule} from "@angular/common/http";

describe('UserHttpService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule],
    }));

    it('should be created', () => {
        const service: UserHttpService = TestBed.get(UserHttpService);
        expect(service).toBeTruthy();
    });
});
