/*
 * Project : ecomics-ng
 * FileName : user-http.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {HandleTokenHttpService} from "../handle-token-http.service";
import {SessionService} from "../../session/session.service";
import {Role, User} from "../../../models";

@Injectable({
    providedIn: 'root'
})
export class UserHttpService {
    baseUrl = environment.apiUrl;

    constructor(private http: HttpClient, private globalHttpService: HandleTokenHttpService, private sessionService: SessionService) {
    }

    getAllUser(): any {
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/user/all?token=${token}`, {}).pipe(
            map((response: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(response);

                let users: User[] = [];
                if (response.succeed) {
                    response.data.forEach(userData => {
                        users.push(User.parse(userData));
                    });
                }

                return users;
            })
        );
    }

    getAllRole(): any {
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/role/all?token=${token}`, {}).pipe(
            map((response: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(response);

                let roles: Role[] = [];
                if (response.succeed) {
                    response.data.forEach(roleData => {
                        roles.push(Role.parse(roleData));
                    });
                }

                return roles;
            })
        );
    }

    createRole(roleName: string): any {
        return this.http.post(`${this.baseUrl}/role`, {
            label: roleName,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    deleteRole(roleId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/role?roleId=${roleId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    addRole(userId: number, roleId: number): any {
        return this.http.post(`${this.baseUrl}/user/role`, {
            userId: userId,
            roleId: roleId,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    removeRole(userId: number, roleId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/user/role?userId=${userId}&roleId=${roleId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    deleteUser(userId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/user?userId=${userId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    loginPassword(email: string, password: string): any {
        return this.http.post(`${this.baseUrl}/user/authenticate`, {
            email: email,
            password: password
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    loginSocial(email: string, socialName: string, socialId: string, firstName: string, lastName: string) {
        return this.http.put(`${this.baseUrl}/user/authenticate`, {
            email: email,
            socialName: socialName,
            socialId: socialId,
            firstName: firstName,
            lastName: lastName
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    register(email: string, password: string, firstName: string, lastName: string) {
        return this.http.post(`${this.baseUrl}/user`, {
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    updateUserData(id: number, email: string, firstName: string, lastName: string) {
        return this.http.put(`${this.baseUrl}/user`, {
            userId: id,
            email: email,
            firstName: firstName,
            lastName: lastName,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data, true);

                return data;
            })
        );
    }

    updateUserPassword(id: number, password: string) {
        return this.http.put(`${this.baseUrl}/user`, {
            userId: id,
            password: password,
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }
}
