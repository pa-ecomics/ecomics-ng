/*
 * Project : ecomics-ng
 * FileName : template-http.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {map} from "rxjs/operators";
import {HandleTokenHttpService} from "../handle-token-http.service";
import {SessionService} from "../..";
import {Filter, Tag, Template} from "../../../models";

@Injectable({
    providedIn: 'root'
})
export class TemplateHttpService {
    baseUrl = environment.apiUrl;

    constructor(private http: HttpClient, private globalHttpService: HandleTokenHttpService, private sessionService: SessionService) {
    }

    getAllTemplate(): any {
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/template/all?token=${token}`, {}).pipe(
            map((response: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(response);

                let templates: Template[] = [];
                if (response.succeed) {
                    response.data.forEach(templateData => {
                        templates.push(Template.parse(templateData));
                    });
                }

                return templates;
            })
        );
    }

    getAllFilter(): any {
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/filter/all?token=${token}`, {}).pipe(
            map((response: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(response);

                let filters: Filter[] = [];
                if (response.succeed) {
                    response.data.forEach(filterData => {
                        filters.push(Filter.parse(filterData));

                    });
                }

                return filters;
            })
        );
    }

    getAllTag(): any {
        let token = this.sessionService.getToken();

        return this.http.get(`${this.baseUrl}/tag/all?token=${token}`, {}).pipe(
            map((response: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(response);

                let tag: Tag[] = [];
                if (response.succeed) {
                    response.data.forEach(tagData => {
                        tag.push(Tag.parse(tagData));

                    });
                }

                return tag;
            })
        );
    }

    createTemplate(title: string, description: string, miniature: File, numberPage: number, filterId: number): any {
        const formData: FormData = new FormData();

        let data = {
            title: title,
            description: description,
            userId: this.sessionService.getUser().id,
            numberPage: numberPage,
            filterId: filterId,
            token: this.sessionService.getToken()
        };

        formData.append('formData', JSON.stringify(data));
        if (miniature) {
            formData.append('miniature', miniature, miniature.name);
        }

        return this.http.post(`${this.baseUrl}/template`, formData).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    updateTemplate(templateId: number, title: string, description: string, miniature: File, filterId: number): any {
        const formData: FormData = new FormData();

        let data = {
            templateId: templateId,
            title: title,
            description: description,
            filterId: filterId,
            userId: this.sessionService.getUser().id,
            token: this.sessionService.getToken()
        };

        formData.append('formData', JSON.stringify(data));
        if (miniature) {
            formData.append('miniature', miniature, miniature.name);
        }

        return this.http.put(`${this.baseUrl}/template`, formData).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    deleteTemplate(templateId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/template?templateId=${templateId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    createTag(tagName: string): any {
        return this.http.post(`${this.baseUrl}/tag`, {
            label: tagName,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    createFilter(filerLabel: string, filterUrl: string): any {
        return this.http.post(`${this.baseUrl}/filter`, {
            filterLabel: filerLabel,
            filterUrl: filterUrl,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    deleteFilter(filterId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/filter?filterId=${filterId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    addTag(templateId: number, tagId: number): any {
        return this.http.post(`${this.baseUrl}/template/tag`, {
            templateId: templateId,
            tagId: tagId,
            token: this.sessionService.getToken()
        }).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    deleteTag(tagId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/tag?tagId=${tagId}&token=${token}`).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }

    removeTag(templateId: number, tagId: number): any {
        let token = this.sessionService.getToken();

        return this.http.delete(`${this.baseUrl}/template/tag?templateId=${templateId}&tagId=${tagId}&token=${token}`,).pipe(
            map((data: { succeed: boolean, data, token: string }) => {
                this.globalHttpService.handleRequestResponse(data);

                return data;
            })
        );
    }
}
