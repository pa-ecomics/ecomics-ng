/*
 * Project : ecomics-ng
 * FileName : template-http.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { TemplateHttpService } from './template-http.service';

describe('TemplateHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplateHttpService = TestBed.get(TemplateHttpService);
    expect(service).toBeTruthy();
  });
});
