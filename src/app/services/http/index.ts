/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {UserHttpService} from './user/user-http.service'
import {TemplateHttpService} from './template/template-http.service'
import {PageHttpService} from './page/page-http.service'
import {BdHttpService} from './bd/bd-http.service'

export {UserHttpService, TemplateHttpService, PageHttpService, BdHttpService}
