/*
 * Project : ecomics-ng
 * FileName : crypto.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class CryptoService {
    key = environment.cryptoKey;

    constructor() {
    }

    encrypt(data: Object|string) {
        return CryptoJS.AES.encrypt(JSON.stringify(data), this.key);
    }

    decrypt(ciphertext: string) {
        if (!ciphertext) {
           return null;
        }

        let bytes  = CryptoJS.AES.decrypt(ciphertext.toString(), this.key);
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8)); /* required toString params !*/
    }
}
