/*
 * Project : ecomics-ng
 * FileName : home-page.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import Bd from "../../models/Bd";
import {BdHttpService} from "../http";

@Injectable({
    providedIn: 'root'
})
export class HomePageService {
    errorMessage: string;
    successMessage: string;
    bds: Bd[];

    constructor(private bdHttp: BdHttpService) {
    }

    initBd() {
        this.bdHttp.getCurrentUserBd().subscribe(bds => {
            this.bds = bds;
        });
    }

    removeBd(bd: Bd) {
        this.bds.splice(this.bds.indexOf(bd), 1);
    }
}
