/*
 * Project : ecomics-ng
 * FileName : home-page.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { HomePageService } from './home-page.service';

describe('HomePageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomePageService = TestBed.get(HomePageService);
    expect(service).toBeTruthy();
  });
});
