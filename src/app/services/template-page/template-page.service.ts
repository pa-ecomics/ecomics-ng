/*
 * Project : ecomics-ng
 * FileName : template-page.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {TemplateHttpService} from "../http";
import {Filter, Tag, Template} from "../../models";

@Injectable({
    providedIn: 'root'
})
export class TemplatePageService {
    templates: Template[];
    selectedTag: Tag;
    selectedTitle: string;
    selectedAuthorName: string;
    errorMessage: string;
    successMessage: string;

    constructor(private httpTemplate: TemplateHttpService) {
    }

    initTemplates() {
        this.httpTemplate.getAllTemplate().subscribe(templates => {
            this.templates = templates;
        });
    }

    selectTagEvent(event) {
        this.selectedTag = event;
    }

    filterTemplate(): Template[] {
        if (!this.selectedTag && !this.selectedTitle && !this.selectedAuthorName) {
            return this.templates;
        }

        return this.templates.filter(template => {
            return ((!this.selectedTag || template.haveTag(this.selectedTag)) && (!this.selectedTitle || template.title.includes(this.selectedTitle)) && (!this.selectedAuthorName || template.authorName.includes(this.selectedAuthorName)));
        });
    }

    removeTagForAllTemplate(tag: Tag) {
        this.templates.forEach(template => {
            template.removeTag(tag);
        })
    }

    updateTemplateById(templateId: number, updatedTemplate: Template, filter: any) {
        this.templates.forEach(template => {
            if (template.id === updatedTemplate.id) {
                template.title = updatedTemplate.title;
                template.description = updatedTemplate.description;
                template.imageUrl = updatedTemplate.imageUrl;
                template.filter = filter;

                return;
            }
        })
    }

    removeAllFilterOnTemplates(filter: Filter){
        this.templates.forEach(template => {
            if (template.filter.id === filter.id) {
                template.filter = null;
            }
        })
    }

    removeTemplate(template: Template) {
        this.templates.splice(this.templates.indexOf(template), 1);
    }
}
