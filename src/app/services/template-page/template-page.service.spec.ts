/*
 * Project : ecomics-ng
 * FileName : template-page.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { TemplatePageService } from './template-page.service';

describe('TemplatePageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplatePageService = TestBed.get(TemplatePageService);
    expect(service).toBeTruthy();
  });
});
