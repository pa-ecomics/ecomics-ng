/*
 * Project : ecomics-ng
 * FileName : modals.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AddTagModalComponent, DeleteTagModalComponent, NewTagModalComponent} from "../../components/modals/tag";
import {AddRoleModalComponent, DeleteRoleModalComponent, NewRoleModalComponent} from "../../components/modals/role";
import {Template, User} from "../../models";
import {NewTemplateModalComponent, UpdateTemplateModalComponent} from "../../components/modals/template";
import {template} from "@babel/core";
import Frame from "../../models/Frame";
import {UpdateFrameModalComponent} from "../../components/modals/frame";
import {CreateFilterModalComponent, DeleteFilterModalComponent} from "../../components/modals/filter";

@Injectable({
    providedIn: 'root'
})
export class ModalsService {

    constructor(private modalService: NgbModal) {
    }

    openModalNewTag() {
        this.modalService.open(NewTagModalComponent);
    }

    openModalDeleteTag() {
        this.modalService.open(DeleteTagModalComponent);
    }

    openModalNewRole() {
        this.modalService.open(NewRoleModalComponent);
    }

    openModalDeleteRole() {
        this.modalService.open(DeleteRoleModalComponent);
    }

    openModalNewFilter() {
        this.modalService.open(CreateFilterModalComponent);
    }

    openModalDeleteFilter() {
        this.modalService.open(DeleteFilterModalComponent);
    }

    openAddTagModal(template: Template) {
        const modal = this.modalService.open(AddTagModalComponent);
        modal.componentInstance.template = template;
    }

    openUserAddRoleModal(user: User) {
        const modal = this.modalService.open(AddRoleModalComponent);
        modal.componentInstance.user = user;
    }

    openModalNewTemplate() {
        this.modalService.open(NewTemplateModalComponent);
    }

    openModalUpdateTemplate(template: Template) {
        const modal = this.modalService.open(UpdateTemplateModalComponent);
        modal.componentInstance.template = template;
    }

    openModalUpdateFrame(frame: Frame, fromTemplate: boolean) {
        const modal = this.modalService.open(UpdateFrameModalComponent);
        modal.componentInstance.frame = frame;
        modal.componentInstance.fromTemplate = fromTemplate;
    }
}
