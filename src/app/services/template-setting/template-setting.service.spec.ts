/*
 * Project : ecomics-ng
 * FileName : template-setting.service.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { TestBed } from '@angular/core/testing';

import { TemplateSettingService } from './template-setting.service';

describe('TemplateSettingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplateSettingService = TestBed.get(TemplateSettingService);
    expect(service).toBeTruthy();
  });
});
