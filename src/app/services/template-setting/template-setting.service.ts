/*
 * Project : ecomics-ng
 * FileName : template-setting.service.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Injectable} from '@angular/core';
import {Template} from "../../models";
import Page from "../../models/Page";
import Bd from "../../models/Bd";

@Injectable({
    providedIn: 'root'
})
export class TemplateSettingService {
    template: Template|Bd;
    currentPage: Page;
    fromTemplate: boolean;

    constructor() {
    }

    initView(template: Template|Bd) {
        this.template = template;
        this.currentPage = this.template.pages[0];
    }

    previewPage() {
        let currentIndex = this.template.pages.indexOf(this.currentPage);
        let previewIndex = currentIndex === 0 ? this.template.pages.length - 1 : currentIndex - 1;

        this.currentPage = this.template.pages[previewIndex];
    }

    nextPage() {
        let currentIndex = this.template.pages.indexOf(this.currentPage);
        let nextIndex = currentIndex < this.template.pages.length - 1 ? currentIndex + 1 : 0;

        this.currentPage = this.template.pages[nextIndex];
    }
}
