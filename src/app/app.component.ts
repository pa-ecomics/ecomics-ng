import {Component} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';
import {RouterService} from "./services/router/router.service";
import {Url} from "./enums";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private routerService: RouterService, private deviceService: DeviceDetectorService) {
        /* TODO : If mobile redirect to donwload app page */
        if (!this.deviceService.isDesktop()) {
            this.routerService.navigate(Url.mobile)
        }
    }
}
