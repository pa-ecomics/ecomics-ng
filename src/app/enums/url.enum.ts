/*
 * Project : ecomics-ng
 * FileName : url.enum.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

export enum Url {
    home = '/',
    login = '/login',
    register = '/register',
    setting = '/setting',
    template = '/template',
    templateSetting = '/template-setting',
    editBd = '/edit-bd',
    users = '/users',
    mobile = '/mobile',
}
