/*
 * Project : ecomics-ng
 * FileName : Page.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import Frame from "./Frame";
import {environment} from "../../environments/environment";

export default class Page {
    private _id: number;
    private _number: number;
    private _templateId: number;
    private _imageUrl: string|ArrayBuffer;
    private _frames: Frame[];

    constructor(id: number, number: number, directoryId: number, fromTemplate: boolean) {
        this._id = id;
        this._number = number;
        // this._imageUrl = `${environment.apiUrl}/api/shared/${fromTemplate ? 'template' : 'bd'}/${directoryId}/page_${number}.png?`;
        this._imageUrl = `${environment.sharedDirectory}/${fromTemplate ? 'template' : 'bd'}/${directoryId}/page_${number}.png?`;
        this._frames = [];
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get number(): number {
        return this._number;
    }

    set number(number: number) {
        this._number = number;
    }

    get templateId(): number {
        return this._templateId;
    }

    set templateId(templateId: number) {
        this._templateId = templateId;
    }

    get imageUrl(): string | ArrayBuffer {
        return this._imageUrl;
    }

    set imageUrl(imageUrl: string | ArrayBuffer) {
        this._imageUrl = imageUrl;
    }

    get frames(): Frame[] {
        return this._frames;
    }

    set frames(value: Frame[]) {
        this._frames = value;
    }

    addFrame(frame: Frame) {
        this._frames.push(frame);
    }

    reloadImage() {
        this._imageUrl += Math.random().toString();
    }

    static parse(data: any, fromTemplate: boolean, bdId?: number): Page {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        let page = new Page(data.id, data.number, fromTemplate ? data.templateId : bdId, fromTemplate);
        if (data.Frames) {
            data.Frames.forEach(frameData => {
                page.addFrame(Frame.parse(frameData))
            });
        }

        return page;
    }
}
