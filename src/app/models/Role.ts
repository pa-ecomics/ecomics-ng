/*
 * Project : ecomics-ng
 * FileName : Role.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

export default class Role {
    private _id: number;
    private _label: string;

    constructor(id: number, label: string) {
        this._id = id;
        this._label = label;
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get label(): string {
        return this._label;
    }

    set label(label: string) {
        this._label = label;
    }

    toString(inString: boolean): string | {id:number, label: string}{
        let data = {id: this.id, label: this.label};

        return inString ? JSON.stringify(data) : data;
    }

    static parse(data: any): Role {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        return new Role(data.id, data.label);
    }
}
