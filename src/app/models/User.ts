/*
 * Project : ecomics-ng
 * FileName : User.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import Role from './Role';
import Tag from "./Tag";

export default class User {
    private _id: number;
    private _email: string;
    private _firstName: string;
    private _lastName: string;
    private _roles: Role[];
    private _googleLogin: boolean;
    private _facebookLogin: boolean;


    constructor(id: number, email: string, firstName: string, lastName: string, googleLogin: boolean, facebookLogin: boolean) {
        this._id = id;
        this._email = email;
        this._firstName = firstName;
        this._lastName = lastName;
        this._roles = [];
        this._googleLogin = googleLogin;
        this._facebookLogin = facebookLogin;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get googleLogin(): boolean {
        return this._googleLogin;
    }

    set googleLogin(value: boolean) {
        this._googleLogin = value;
    }

    get facebookLogin(): boolean {
        return this._facebookLogin;
    }

    set facebookLogin(value: boolean) {
        this._facebookLogin = value;
    }

    get roles(): Role[] {
        return this._roles;
    }

    set roles(roles: Role[]) {
        this._roles = roles;
    }

    addRole(role: Role) {
        this._roles.push(role);
    }

    removeRole(searchRole: Role) {
        let roleIndex = 0;
        let result = false;

        this.roles.forEach(currentRole => {
            if (currentRole.id === searchRole.id) {
                return result = true;
            }
            roleIndex++;
        });

        if (result) {
            this.roles.splice(roleIndex, 1);
        }
    }

    haveRole(roleName: string): boolean {
        let result = false;
        roleName = roleName.toLowerCase();

        this.roles.forEach(role => {
            if (role.label.toLowerCase() === roleName) {
                return result = true;
            }
        });

        return result;
    }

    toString(): string {
        let data = {id: this.id, email: this.email, firstName: this.firstName, lastName: this.lastName, googleLogin: this.googleLogin, facebookLogin: this.facebookLogin, Roles: []};

        this.roles.forEach(role => {
            data.Roles.push(role.toString(false));
        });

        return JSON.stringify(data);
    }

    static parse(data: any): any {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        let user = new User(data.id, data.email, data.firstName, data.lastName, data.googleId, data.facebookId);

        if (data.Roles) {
            data.Roles.forEach(roleData => {
                user.addRole(Role.parse(roleData));
            });
        }

        return user;
    }
}
