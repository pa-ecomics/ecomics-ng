/*
 * Project : ecomics-ng
 * FileName : Bd.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {environment} from "../../environments/environment";
import Template from "./Template";
import Page from "./Page";

export default class Bd {
    private _id: number;
    private _imageUrl: string|ArrayBuffer;
    private _createdAt: Date;
    private _updatedAt: Date;
    private _template: Template;
    private _title: string;
    private _pages: Page[];

    constructor(id: number, createdAt: Date, updatedAt: Date) {
        this._id = id;
        this._createdAt = createdAt;
        this._updatedAt = updatedAt;
        // this._imageUrl = `${environment.apiUrl}/api/shared/bd/${id}/miniature.png?`;
        this._imageUrl = `${environment.sharedDirectory}/bd/${id}/miniature.png?`;
        this._pages = [];
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get imageUrl(): string|ArrayBuffer {
        return this._imageUrl;
    }

    set imageUrl(imageUrl: string|ArrayBuffer) {
        this._imageUrl = imageUrl;
    }

    get template(): Template {
        return this._template;
    }

    set template(value: Template) {
        this._template = value;
    }

    get createdAt(): Date {
        return this._createdAt;
    }

    set create(createdAt: Date) {
        this._createdAt = createdAt;
    }

    get updatedAt(): Date {
        return this._updatedAt;
    }

    set updatedAt(updatedAt: Date) {
        this._updatedAt = updatedAt;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }
    get pages(): Page[] {
        return this._pages;
    }

    set pages(value: Page[]) {
        this._pages = value;
    }

    addPage(page: Page): void {
        this._pages.push(page);
    }

    static parse(data: any): Bd {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        let bd = new Bd(data.id, data.createdAt, data.updatedAt);
        if (data.Template) {
            bd._template = Template.parse(data.Template);

            if (data.Template.Pages) {
                data.Template.Pages.forEach(pageData => {
                    bd.addPage(Page.parse(pageData, false, bd.id));
                });
            }
        }

        return bd;
    }
}
