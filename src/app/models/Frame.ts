/*
 * Project : ecomics-ng
 * FileName : Frame.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

export default class Frame {
    private _id: number;
    private _positionX: number;
    private _positionY: number;
    private _width: number;
    private _height: number;
    private _indication: string;
    private _isTaken: boolean;

    constructor(id: number, positionX: number, positionY: number, width: number, height: number, indication: string, isTaken: boolean) {
        this._id = id;
        this._positionX = positionX;
        this._positionY = positionY;
        this._width = width;
        this._height = height;
        this._indication = indication;
        this._isTaken = isTaken;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get positionX(): number {
        return this._positionX;
    }

    set positionX(value: number) {
        this._positionX = value;
    }

    get positionY(): number {
        return this._positionY;
    }

    set positionY(value: number) {
        this._positionY = value;
    }

    get width(): number {
        return this._width;
    }

    set width(value: number) {
        this._width = value;
    }

    get height(): number {
        return this._height;
    }

    set height(value: number) {
        this._height = value;
    }

    get indication(): string {
        return this._indication;
    }

    set indication(value: string) {
        this._indication = value;
    }

    get isTaken(): boolean {
        return this._isTaken;
    }

    set isTaken(value: boolean) {
        this._isTaken = value;
    }

    static parse(data: any): Frame {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        return new Frame(data.id, data.positionX, data.positionY, data.width, data.height, data.indication, data.isTaken);
    }
}
