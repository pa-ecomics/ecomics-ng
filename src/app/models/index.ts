/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import User from './User';
import Role from './Role';
import Template from './Template';
import Tag from './Tag';
import Filter from './Filter';

export {User, Role, Template, Tag, Filter};
