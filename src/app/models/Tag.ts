/*
 * Project : ecomics-ng
 * FileName : Tag.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

export default class Tag {
    private _id: number;
    private _label: string;

    constructor(id: number, label: string) {
        this._id = id;
        this._label = label;
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get label(): string {
        return this._label;
    }

    set label(label: string) {
        this._label = label;
    }

    static parse(data: any): Tag {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        return new Tag(data.id, data.label);
    }
}
