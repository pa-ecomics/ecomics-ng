/*
 * Project : ecomics-ng
 * FileName : Template.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import Tag from "./Tag";
import {environment} from "../../environments/environment";
import Page from "./Page";
import Filter from "./Filter";

export default class Template {
    private _id: number;
    private _title: string;
    private _description: string;
    private _authorName: string;
    private _imageUrl: string|ArrayBuffer;
    private _createdAt: Date;
    private _updatedAt: Date;
    private _tags: Tag[];
    private _pages: Page[];
    private _filter: any;

    constructor(id: number, title: string, description: string, authorName: string, createdAt: Date, updatedAt: Date) {
        this._id = id;
        this._title = title;
        this._description = description;
        this._authorName = authorName;
        // this._imageUrl = `${environment.apiUrl}/api/shared/template/${id}/miniature.png?`;
        this._imageUrl = `${environment.sharedDirectory}/template/${id}/miniature.png?${Math.random()}`;
        this._createdAt = createdAt;
        this._updatedAt = updatedAt;
        this._tags = [];
        this._pages = [];
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get title(): string {
        return this._title;
    }

    set title(title: string) {
        this._title = title;
    }

    get description(): string {
        return this._description;
    }

    set description(description: string) {
        this._description = description;
    }

    get authorName(): string {
        return this._authorName;
    }

    set authorName(authorName: string) {
        this._authorName = authorName;
    }

    get imageUrl(): string|ArrayBuffer {
        return this._imageUrl;
    }

    set imageUrl(imageUrl: string|ArrayBuffer) {
        this._imageUrl = imageUrl;
    }

    get createdAt(): Date {
        return this._createdAt;
    }

    set create(createdAt: Date) {
        this._createdAt = createdAt;
    }

    get updatedAt(): Date {
        return this._updatedAt;
    }

    set updatedAt(updatedAt: Date) {
        this._updatedAt = updatedAt;
    }

    get tags(): Tag[] {
        return this._tags;
    }

    set tags(tags: Tag[]) {
        this._tags = tags;
    }

    get pages(): Page[] {
        return this._pages;
    }

    set pages(pages: Page[]) {
        this._pages = pages;
    }

    get filter(): any {
        return this._filter;
    }

    set filter(value: any) {
        this._filter = value;
    }

    reloadImage(): string|ArrayBuffer{
        this._imageUrl = `${environment.sharedDirectory}/template/${this.id}/miniature.png?${Math.random()}`;
        // this._imageUrl += Math.random().toString();
        return this._imageUrl;
    }

    addTag(tag: Tag): void {
        this._tags.push(tag);
    }

    addPage(page: Page): void {
        this._pages.push(page);
    }

    removeTag(searchTag: Tag): void {
        let tagIndex = 0;
        let result = false;

        this.tags.forEach(currentTag => {
            if (currentTag.id === searchTag.id) {
                return result = true;
            }
            tagIndex++;
        });

        if (result) {
            this.tags.splice(tagIndex, 1);
        }
    }

    haveTag(tagSearch: Tag): boolean {
        let result = false;

        this.tags.forEach(tag => {
            if (tag.id === tagSearch.id) {
                return result = true;
            }
        });

        return result;
    }

    static parse(data: any): Template {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        let authorName;
        if (data.User && (data.User.firstName || data.User.lastName)) {
                authorName = `${data.User.firstName + ' ' || ''}${data.User.lastName || ''}`;
        }

        let template = new Template(data.id, data.title, data.description, authorName, data.createdAt, data.updatedAt);
        if (data.Tags) {
            data.Tags.forEach(tagData => {
                template.addTag(Tag.parse(tagData));
            });
        }

        if (data.Pages) {
            data.Pages.forEach(pageData => {
                template.addPage(Page.parse(pageData, true));
            });
        }

        if (data.Filter) {
            template._filter = Filter.parse(data.Filter);
        }

        return template;
    }
}
