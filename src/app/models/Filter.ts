/*
 * Project : ecomics-ng
 * FileName : Filter.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

export default class Filter {
    private _id: number;
    private _label: string;
    private _url: string;

    constructor(id: number, label: string, url: string) {
        this._id = id;
        this._label = label;
        this._url = url;
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value;
    }

    get url(): string {
        return this._url;
    }

    set url(value: string) {
        this._url = value;
    }

    static parse(data: any): Filter {
        if (!data) {
            return null;
        } else if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        return new Filter(data.id, data.label, data.url);
    }
}
