/*
 * Project : ecomics-ng
 * FileName : frame-style.directive.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import Frame from "../../models/Frame";

@Directive({
    selector: '[appFrameStyle]'
})
export class FrameStyleDirective implements OnInit{
    @Input() frame: Frame;

    constructor(private element: ElementRef) {
    }

    ngOnInit(): void {
        this.element.nativeElement.style.top = `${this.frame.positionY / 2}px`;
        this.element.nativeElement.style.left = `${this.frame.positionX / 2}px`;
        this.element.nativeElement.style.width = `${this.frame.width / 2}px`;
        this.element.nativeElement.style.height = `${this.frame.height / 2}px`;
    }

}
