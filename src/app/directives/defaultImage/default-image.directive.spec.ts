/*
 * Project : ecomics-ng
 * FileName : default-image.directive.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { DefaultImageDirective } from './default-image.directive';

describe('DefaultImageDirective', () => {
  it('should create an instance', () => {
    const directive = new DefaultImageDirective();
    expect(directive).toBeTruthy();
  });
});
