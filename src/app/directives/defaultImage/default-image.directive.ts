/*
 * Project : ecomics-ng
 * FileName : default-image.directive.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Directive, Input} from '@angular/core';

@Directive({
    selector: '[appDefaultImage]',
    host: {
        '(error)': 'updateUrl()',
        '[src]': 'src'
    }
})
export class DefaultImageDirective {
    @Input() src: string;
    @Input() type: string;
    @Input() default: string;

    constructor() {
    }

    updateUrl() {
        switch (this.type) {
            case 'template':
                this.src = 'assets/default/template.png';
                break;
            case 'frame':
                this.src = 'assets/default/frame.png';
                break;
            default:
                this.src = this.default;
        }
    }
}
