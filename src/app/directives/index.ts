/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {DefaultImageDirective} from './defaultImage/default-image.directive';
import {FrameStyleDirective} from './frame-style/frame-style.directive';

export { DefaultImageDirective, FrameStyleDirective };
