/*
 * Project : ecomics-ng
 * FileName : auth.module.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider} from "angularx-social-login";
import {environment} from "../../environments/environment";

export function provideConfig(): AuthServiceConfig {
    return new AuthServiceConfig([
        {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleClientId)
        },
        {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookClientId)
        }
    ]);
}

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ]
})
export class AuthModule {
}
