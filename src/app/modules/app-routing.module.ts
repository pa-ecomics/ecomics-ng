/*
 * Project : ecomics-ng
 * FileName : app-routing.module.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
    EditBdPageComponent,
    HomePageComponent,
    LoginPageComponent,
    MobilePageComponent,
    RegisterPageComponent,
    SettingPageComponent,
    TemplatePageComponent,
    TemplateSettingPageComponent,
    UsersPageComponent
} from "../components/pages";
import {Url} from "../enums";

export const appRouteList: Routes = [
    {
        path: Url.home.substr(1),
        component: HomePageComponent
    },
    {
        path: Url.login.substr(1),
        component: LoginPageComponent
    },
    {
        path: Url.register.substr(1),
        component: RegisterPageComponent
    },
    {
        path: Url.setting.substr(1),
        component: SettingPageComponent
    },
    {
        path: Url.template.substr(1),
        component: TemplatePageComponent
    },
    {
        path: Url.templateSetting.substr(1),
        component: TemplateSettingPageComponent
    },
    {
        path: Url.editBd.substr(1),
        component: EditBdPageComponent
    },
    {
        path: Url.users.substr(1),
        component: UsersPageComponent
    },
    {
        path: Url.mobile.substr(1),
        component: MobilePageComponent
    },
    {
        path: '**',
        redirectTo: 'login'
    }
];

@NgModule({
    exports: [
        RouterModule
    ],
    imports: [
        RouterModule.forRoot(appRouteList)
    ]
})
export class AppRoutingModule {
}
