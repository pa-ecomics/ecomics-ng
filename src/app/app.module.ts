import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AuthServiceConfig, SocialLoginModule} from 'angularx-social-login';

import {AppComponent} from './app.component';
import {AppRoutingModule, provideConfig} from './modules';
import {UpdateFrameModalComponent} from './components/modals/frame';
import {AddTagModalComponent, DeleteTagModalComponent, NewTagModalComponent} from './components/modals/tag';
import {CreateFilterModalComponent, DeleteFilterModalComponent} from './components/modals/filter';
import {AddRoleModalComponent, DeleteRoleModalComponent, NewRoleModalComponent} from './components/modals/role';
import {NewTemplateModalComponent, UpdateTemplateModalComponent} from './components/modals/template';
import {BdViewComponent, FilterSelectComponent, PageViewComponent, RoleSelectComponent, TagSelectComponent, TemplateViewComponent, UsersListTableComponent, DownloadApkComponent} from './components/views';
import {HeaderIncludeComponent} from './components/pages/includes/header-include/header-include.component';
import {LoginFormComponent, NewTemplateFormComponent, RegisterFormComponent, UpdatePasswordFormComponent, UpdateUserFormComponent} from './components/forms';
import {
    EditBdPageComponent,
    HomePageComponent,
    LoginPageComponent,
    RegisterPageComponent,
    SettingPageComponent,
    TemplatePageComponent,
    TemplateSettingPageComponent,
    UsersPageComponent,
    MobilePageComponent
} from './components/pages';
import {DefaultImageDirective, FrameStyleDirective} from './directives';
import { DeviceDetectorModule } from 'ngx-device-detector';


@NgModule({
    declarations: [
        AppComponent,
        LoginPageComponent,
        RegisterPageComponent,
        HomePageComponent,
        HomePageComponent,
        HeaderIncludeComponent,
        LoginFormComponent,
        RegisterFormComponent,
        SettingPageComponent,
        TemplatePageComponent,
        UpdateUserFormComponent,
        UpdatePasswordFormComponent,
        TemplateViewComponent,
        TagSelectComponent,
        NewTagModalComponent,
        UsersPageComponent,
        AddTagModalComponent,
        DeleteTagModalComponent,
        UsersListTableComponent,
        AddRoleModalComponent,
        RoleSelectComponent,
        DeleteRoleModalComponent,
        NewRoleModalComponent,
        NewTemplateFormComponent,
        DefaultImageDirective,
        NewTemplateModalComponent,
        UpdateTemplateModalComponent,
        TemplateSettingPageComponent,
        PageViewComponent,
        FrameStyleDirective,
        UpdateFrameModalComponent,
        BdViewComponent,
        EditBdPageComponent,
        FilterSelectComponent,
        CreateFilterModalComponent,
        DeleteFilterModalComponent,
        MobilePageComponent,
        DownloadApkComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgbModule,
        FormsModule,
        HttpClientModule,
        SocialLoginModule,
        ReactiveFormsModule,
        DeviceDetectorModule.forRoot()
    ],
    providers: [{
        provide: AuthServiceConfig,
        useFactory: provideConfig
    }],
    entryComponents: [
        NewTagModalComponent,
        AddTagModalComponent,
        DeleteTagModalComponent,
        AddRoleModalComponent,
        DeleteRoleModalComponent,
        NewRoleModalComponent,
        NewTemplateModalComponent,
        UpdateTemplateModalComponent,
        UpdateFrameModalComponent,
        CreateFilterModalComponent,
        DeleteFilterModalComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
