/*
 * Project : ecomics-ng
 * FileName : template-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {TemplatePageService} from '../../../services/template-page/template-page.service';
import {Url} from '../../../enums';
import {RouterService} from '../../../services/router/router.service';
import {HeaderService} from '../../../services/header/header.service';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-template-page',
    templateUrl: './template-page.component.html',
    styleUrls: ['./template-page.component.scss'],
    animations: [fadeInOut]
})
export class TemplatePageComponent implements OnInit {

    constructor(private templateService: TemplatePageService, private headerService: HeaderService) {
    }

    ngOnInit() {
        this.headerService.currentUrl = Url.template;
        this.templateService.initTemplates();
    }
}
