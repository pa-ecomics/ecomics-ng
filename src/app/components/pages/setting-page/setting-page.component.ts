/*
 * Project : ecomics-ng
 * FileName : setting-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {HeaderService} from "../../../services/header/header.service";
import {Url} from "../../../enums";

@Component({
    selector: 'app-setting-page',
    templateUrl: './setting-page.component.html',
    styleUrls: ['./setting-page.component.scss']
})
export class SettingPageComponent implements OnInit {

    constructor(private headerService: HeaderService) {
    }

    ngOnInit() {
        this.headerService.currentUrl = Url.setting;
    }
}
