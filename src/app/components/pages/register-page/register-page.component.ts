/*
 * Project : ecomics-ng
 * FileName : register-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {SessionService} from "../../../services";
import {Url} from "../../../enums";
import {RouterService} from "../../../services/router/router.service";
import {HeaderService} from "../../../services/header/header.service";

@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {
    url = Url;

    constructor(private Session: SessionService, private headerService: HeaderService) {
        this.Session.closeSession()
    }

    ngOnInit() {
        this.headerService.currentUrl = Url.login;
    }

}
