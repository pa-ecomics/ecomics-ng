/*
 * Project : ecomics-ng
 * FileName : edit-bd-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {RouterService} from "../../../services/router/router.service";
import {Url} from "../../../enums";
import {EditBdService} from "../../../services/edit-bd/edit-bd.service";
import {TemplatePageService} from "../../../services/template-page/template-page.service";
import {HeaderService} from "../../../services/header/header.service";

@Component({
    selector: 'app-edit-bd-page',
    templateUrl: './edit-bd-page.component.html',
    styleUrls: ['./edit-bd-page.component.scss']
})
export class EditBdPageComponent implements OnInit {

    constructor(private editBdService: EditBdService, private routerService: RouterService, private headerService: HeaderService) {
    }

    ngOnInit() {
        this.headerService.currentUrl = Url.editBd;

        if (!this.editBdService.bd) {
            this.routerService.navigate(Url.home)
        }
    }

}
