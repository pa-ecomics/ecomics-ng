/*
 * Project : ecomics-ng
 * FileName : edit-bd-page.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBdPageComponent } from './edit-bd-page.component';

describe('EditBdPageComponent', () => {
  let component: EditBdPageComponent;
  let fixture: ComponentFixture<EditBdPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBdPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBdPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
