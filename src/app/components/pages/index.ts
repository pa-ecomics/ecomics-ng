/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {LoginPageComponent} from './login-page/login-page.component'
import {RegisterPageComponent} from './register-page/register-page.component'
import {HomePageComponent} from './home-page/home-page.component'
import {SettingPageComponent} from './setting-page/setting-page.component'
import {TemplatePageComponent} from './template-page/template-page.component'
import {UsersPageComponent} from './users-page/users-page.component'
import {TemplateSettingPageComponent} from './template-setting-page/template-setting-page.component'
import {EditBdPageComponent} from './edit-bd-page/edit-bd-page.component'
import {MobilePageComponent} from './mobile-page/mobile-page.component'

export {LoginPageComponent, RegisterPageComponent, HomePageComponent, SettingPageComponent, TemplatePageComponent, UsersPageComponent, TemplateSettingPageComponent, EditBdPageComponent, MobilePageComponent}
