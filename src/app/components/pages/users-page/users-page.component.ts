/*
 * Project : ecomics-ng
 * FileName : users-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { Component, OnInit } from '@angular/core';
import {HeaderService} from "../../../services/header/header.service";
import {Url} from "../../../enums";

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.currentUrl = Url.users;
  }
}
