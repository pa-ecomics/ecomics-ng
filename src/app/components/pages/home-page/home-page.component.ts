/*
 * Project : ecomics-ng
 * FileName : home-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {HomePageService} from '../../../services/home-page/home-page.service';
import {HeaderService} from '../../../services/header/header.service';
import {Url} from '../../../enums';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss'],
    animations: [fadeInOut]
})
export class HomePageComponent implements OnInit {

    constructor(private homePageService: HomePageService, private headerService: HeaderService) {
    }

    ngOnInit() {
        this.headerService.currentUrl = Url.home;
        this.homePageService.initBd();
    }
}
