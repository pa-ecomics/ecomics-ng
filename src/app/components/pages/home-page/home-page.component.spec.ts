/*
 * Project : ecomics-ng
 * FileName : home-page.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomePageComponent} from './home-page.component';
import {HeaderIncludeComponent} from "../includes/header-include/header-include.component";
import {RouterTestingModule} from "@angular/router/testing";

describe('HomePageComponent', () => {
    let component: HomePageComponent;
    let fixture: ComponentFixture<HomePageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HomePageComponent, HeaderIncludeComponent],
            imports: [RouterTestingModule]

        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomePageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
