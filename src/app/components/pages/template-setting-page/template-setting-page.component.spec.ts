/*
 * Project : ecomics-ng
 * FileName : template-setting-page.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateSettingPageComponent } from './template-setting-page.component';

describe('TemplateSettingPageComponent', () => {
  let component: TemplateSettingPageComponent;
  let fixture: ComponentFixture<TemplateSettingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateSettingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateSettingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
