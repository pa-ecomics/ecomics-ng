/*
 * Project : ecomics-ng
 * FileName : template-setting-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {TemplateSettingService} from "../../../services/template-setting/template-setting.service";
import {RouterService} from "../../../services/router/router.service";
import {Url} from "../../../enums";
import {HeaderService} from "../../../services/header/header.service";

@Component({
    selector: 'app-template-setting-page',
    templateUrl: './template-setting-page.component.html',
    styleUrls: ['./template-setting-page.component.scss']
})
export class TemplateSettingPageComponent implements OnInit {
    constructor(private templateSettingService: TemplateSettingService, private routerService: RouterService, private headerService: HeaderService) {

    }

    ngOnInit() {
        this.headerService.currentUrl = Url.templateSetting;

        if (!this.templateSettingService.template) {
            this.routerService.navigate(Url.template)
        }
    }
}
