/*
 * Project : ecomics-ng
 * FileName : header-include.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {SessionService} from "../../../../services";
import {HeaderService} from "../../../../services/header/header.service";
import {RouterService} from "../../../../services/router/router.service";
import {Url} from "../../../../enums";
import {ModalsService} from "../../../../services/modals/modals.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-header-include',
    templateUrl: './header-include.component.html',
    styleUrls: ['./header-include.component.scss'],
})
export class HeaderIncludeComponent implements OnInit {
    url: Url;

    constructor(private sessionService: SessionService, private routerService: RouterService, private headerService: HeaderService, private modalsService: ModalsService) {
    }

    ngOnInit() {
        this.sessionService.checkSession();
        this.headerService.user = this.sessionService.getUser();
    }
}
