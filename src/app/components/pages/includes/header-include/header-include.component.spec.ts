/*
 * Project : ecomics-ng
 * FileName : header-include.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderIncludeComponent} from './header-include.component';
import {RouterTestingModule} from "@angular/router/testing";

describe('HeaderIncludeComponent', () => {
    let component: HeaderIncludeComponent;
    let fixture: ComponentFixture<HeaderIncludeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HeaderIncludeComponent],
            imports: [RouterTestingModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderIncludeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
