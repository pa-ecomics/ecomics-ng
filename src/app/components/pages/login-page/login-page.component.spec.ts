/*
 * Project : ecomics-ng
 * FileName : login-page.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginPageComponent} from './login-page.component';
import {LoginFormComponent} from "../../forms/login/login-form.component";
import {NgbAlert} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {AuthService, AuthServiceConfig} from "angularx-social-login";
import {provideConfig} from "../../../modules";

describe('LoginPageComponent', () => {
    let component: LoginPageComponent;
    let fixture: ComponentFixture<LoginPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginPageComponent, LoginFormComponent, NgbAlert],
            imports: [FormsModule, RouterTestingModule, HttpClientModule],
            providers: [AuthService, {provide: AuthServiceConfig, useFactory: provideConfig}],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
