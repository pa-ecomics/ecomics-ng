/*
 * Project : ecomics-ng
 * FileName : login-page.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../../services';
import {Url} from "../../../enums";
import {HeaderService} from "../../../services/header/header.service";

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
    url = Url;

    constructor(private Session: SessionService, private headerService: HeaderService) {
        this.Session.closeSession()
    }

    ngOnInit() {
        this.headerService.currentUrl = Url.login;
    }
}
