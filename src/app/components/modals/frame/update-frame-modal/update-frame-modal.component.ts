/*
 * Project : ecomics-ng
 * FileName : update-frame-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import Frame from "../../../../models/Frame";
import {PageHttpService} from "../../../../services/http";
import {TemplateSettingService} from "../../../../services/template-setting/template-setting.service";
import {EditBdService} from "../../../../services/edit-bd/edit-bd.service";
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-update-frame-modal',
    templateUrl: './update-frame-modal.component.html',
    styleUrls: ['./update-frame-modal.component.scss'],
    animations: [fadeInOut]
})
export class UpdateFrameModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    frame: Frame;
    indication: string;
    imagePreview: any;
    frameImage: File = null;
    fromTemplate: boolean;
    loading: boolean;


    constructor(private activeModal: NgbActiveModal, private pageHttp: PageHttpService, private templateSettingService: TemplateSettingService, private editBdService: EditBdService) {

    }

    ngOnInit() {
        this.indication = this.frame.indication;
    }

    isFormDisabled() {
        return (!this.indication || this.indication === this.frame.indication) && (!this.frameImage)
    }

    updateFrame() {
        if (this.fromTemplate) {
            this.loading = true;
            this.pageHttp.updateTemplateFrame(this.frame.id, this.indication, this.frameImage).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = 'Frame updated !';

                    this.frame.indication = this.indication;
                    this.frame.isTaken = !!this.frameImage;
                    this.templateSettingService.currentPage.reloadImage();
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                this.loading = false;
                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            });
        } else {
            this.loading = true;

            const startFrom = new Date().getTime();
            this.pageHttp.updateBdFrame(this.editBdService.bd.id, this.frame.id, this.frameImage).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = 'Frame updated !';
                    this.editBdService.currentPage.reloadImage();
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                this.loading = false;
                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            }, error => {
                // console.log(error);
                // console.log('Request Error execution time: %dms', new Date().getTime() - startFrom);
            });
        }
    }

    removeFrameImage() {
        if (this.fromTemplate) {
            this.pageHttp.removeFrameImage(this.frame.id).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = "Frame Image removed !";
                    this.templateSettingService.currentPage.reloadImage();
                    this.frame.isTaken = false;
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            });
        } else {
            this.pageHttp.removeBdFrame(this.editBdService.bd.id, this.frame.id).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = "Frame Image removed !";
                    this.editBdService.currentPage.reloadImage();
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            });
        }
    }

    handleFileInput(event: any) {
        if (event.target && event.target.files) {
            this.frameImage = event.target.files[0];

            let reader = new FileReader();
            reader.readAsDataURL(this.frameImage);
            reader.onload = (_event) => {
                this.imagePreview = reader.result;
            }
        }
    }
}
