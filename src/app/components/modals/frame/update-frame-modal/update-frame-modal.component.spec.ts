/*
 * Project : ecomics-ng
 * FileName : update-frame-modal.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFrameModalComponent } from './update-frame-modal.component';

describe('UpdateFrameModalComponent', () => {
  let component: UpdateFrameModalComponent;
  let fixture: ComponentFixture<UpdateFrameModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFrameModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFrameModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
