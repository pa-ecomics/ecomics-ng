/*
 * Project : ecomics-ng
 * FileName : add-tag-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TemplateHttpService} from '../../../../services/http/template/template-http.service';
import {Tag, Template} from '../../../../models';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-add-tag-modal',
    templateUrl: './add-tag-modal.component.html',
    styleUrls: ['./add-tag-modal.component.scss'],
    animations: [fadeInOut]
})
export class AddTagModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    selectedTag: Tag;
    template: Template;

    constructor(private activeModal: NgbActiveModal, private templateHttp: TemplateHttpService) {
    }

    ngOnInit() {
    }

    selectTagEvent(event) {
        this.selectedTag = event;
    }

    addTag() {
        this.templateHttp.addTag(this.template.id, this.selectedTag.id).subscribe((response) => {
            if (response.succeed) {
                this.errorMessage = null;
                this.successMessage = 'Tag added !';
                this.template.addTag(this.selectedTag);
            } else {
                this.errorMessage = response.message;
                this.successMessage = null;
            }

            setTimeout(() => {
                this.successMessage = null;
                this.errorMessage = null;
            }, 4000);
        });
    }
}
