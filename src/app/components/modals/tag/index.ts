/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {NewTagModalComponent} from './new-tag-modal/new-tag-modal.component'
import {AddTagModalComponent} from './add-tag-modal/add-tag-modal.component'
import {DeleteTagModalComponent} from './delete-tag-modal/delete-tag-modal.component'

export {NewTagModalComponent, AddTagModalComponent, DeleteTagModalComponent}
