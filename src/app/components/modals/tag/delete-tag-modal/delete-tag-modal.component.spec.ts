/*
 * Project : ecomics-ng
 * FileName : delete-tag-modal.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTagModalComponent } from './delete-tag-modal.component';

describe('DeleteTagModalComponent', () => {
  let component: DeleteTagModalComponent;
  let fixture: ComponentFixture<DeleteTagModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTagModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTagModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
