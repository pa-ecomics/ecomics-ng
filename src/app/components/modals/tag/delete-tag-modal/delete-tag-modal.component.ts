/*
 * Project : ecomics-ng
 * FileName : delete-tag-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TemplateHttpService} from '../../../../services/http/template/template-http.service';
import {Tag} from '../../../../models';
import {TagSelectService} from '../../../../services/tag-select/tag-select.service';
import {TemplatePageService} from '../../../../services/template-page/template-page.service';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-delete-tag-modal',
    templateUrl: './delete-tag-modal.component.html',
    styleUrls: ['./delete-tag-modal.component.scss'],
    animations: [fadeInOut]
})
export class DeleteTagModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    selectedTag: Tag;

    constructor(private activeModal: NgbActiveModal, private templateHttp: TemplateHttpService, private selectTagService: TagSelectService, private templateService: TemplatePageService) {
    }

    ngOnInit() {
    }

    selectTagEvent(event) {
        this.selectedTag = event;
    }

    deleteTag() {
        if (window.confirm('Delete this tag ?')) {
            this.templateHttp.deleteTag(this.selectedTag.id).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = 'Tag deleted !';
                    this.selectTagService.removeTag(this.selectedTag);
                    this.templateService.removeTagForAllTemplate(this.selectedTag);
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            });
        }
    }
}
