/*
 * Project : ecomics-ng
 * FileName : new-tag-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TemplateHttpService} from '../../../../services/http';
import {Tag} from '../../../../models';
import {TagSelectService} from '../../../../services/tag-select/tag-select.service';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-new-tag-modal',
    templateUrl: './new-tag-modal.component.html',
    styleUrls: ['./new-tag-modal.component.scss'],
    animations: [fadeInOut]
})
export class NewTagModalComponent implements OnInit {
    tagName: string;
    errorMessage: string;
    successMessage: string;

    constructor(private activeModal: NgbActiveModal, private templateHttp: TemplateHttpService, private selectTagService: TagSelectService) {
    }

    ngOnInit() {

    }

    createTag() {
        this.templateHttp.createTag(this.tagName).subscribe((response) => {
            if (response.succeed) {
                this.errorMessage = null;
                this.successMessage = 'Tag Created !';
                this.tagName = null;

                this.selectTagService.tags.push(Tag.parse(response.data));
            } else {
                this.errorMessage = response.message;
                this.successMessage = null;
            }

            setTimeout(() => {
                this.successMessage = null;
                this.errorMessage = null;
            }, 4000);
        });
    }
}
