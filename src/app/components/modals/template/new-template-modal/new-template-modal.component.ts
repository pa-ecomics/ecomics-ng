/*
 * Project : ecomics-ng
 * FileName : new-template-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {TemplateModelService} from "../../../../services/templateModel/template-model.service";

@Component({
    selector: 'app-new-template-modal',
    templateUrl: './new-template-modal.component.html',
    styleUrls: ['./new-template-modal.component.scss']
})
export class NewTemplateModalComponent implements OnInit {

    constructor(private activeModal: NgbActiveModal, private templateModelService: TemplateModelService) {
    }

    ngOnInit() {
        this.templateModelService.initNewTemplate();
    }

}
