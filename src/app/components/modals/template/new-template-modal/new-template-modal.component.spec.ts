/*
 * Project : ecomics-ng
 * FileName : new-template-modal.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTemplateModalComponent } from './new-template-modal.component';

describe('NewTemplateModalComponent', () => {
  let component: NewTemplateModalComponent;
  let fixture: ComponentFixture<NewTemplateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTemplateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTemplateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
