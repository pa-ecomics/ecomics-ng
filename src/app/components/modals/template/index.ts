/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {NewTemplateModalComponent} from './new-template-modal/new-template-modal.component'
import {UpdateTemplateModalComponent} from './update-template-modal/update-template-modal.component'

export {NewTemplateModalComponent, UpdateTemplateModalComponent}
