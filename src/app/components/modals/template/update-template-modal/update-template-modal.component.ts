/*
 * Project : ecomics-ng
 * FileName : update-template-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {TemplateModelService} from "../../../../services/templateModel/template-model.service";
import {Template} from "../../../../models";

@Component({
    selector: 'app-update-template-modal',
    templateUrl: './update-template-modal.component.html',
    styleUrls: ['./update-template-modal.component.scss']
})
export class UpdateTemplateModalComponent implements OnInit {
    template: Template;

    constructor(private activeModal: NgbActiveModal, private templateModelService: TemplateModelService) {
    }

    ngOnInit() {
        this.templateModelService.initUpdateTemplate(this.template);
    }

}
