/*
 * Project : ecomics-ng
 * FileName : update-template-modal.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTemplateModalComponent } from './update-template-modal.component';

describe('UpdateTemplateModalComponent', () => {
  let component: UpdateTemplateModalComponent;
  let fixture: ComponentFixture<UpdateTemplateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTemplateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTemplateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
