/*
 * Project : ecomics-ng
 * FileName : new-role-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Role} from '../../../../models';
import {UserHttpService} from '../../../../services/http';
import {RoleSelectService} from '../../../../services/role-select/role-select.service';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-new-role-modal',
    templateUrl: './new-role-modal.component.html',
    styleUrls: ['./new-role-modal.component.scss'],
    animations: [fadeInOut]
})
export class NewRoleModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    roleName: string;

    constructor(private activeModal: NgbActiveModal, private userHttp: UserHttpService, private selectTagService: RoleSelectService) {
    }

    ngOnInit() {
    }

    createRole() {
        this.userHttp.createRole(this.roleName).subscribe((response) => {
            if (response.succeed) {
                this.errorMessage = null;
                this.successMessage = 'Role Created !';
                this.roleName = null;

                this.selectTagService.roles.push(Role.parse(response.data));
            } else {
                this.errorMessage = response.message;
                this.successMessage = null;
            }

            setTimeout(() => {
                this.successMessage = null;
                this.errorMessage = null;
            }, 4000);
        });
    }

}
