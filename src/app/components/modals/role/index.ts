/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {AddRoleModalComponent} from './add-role-modal/add-role-modal.component'
import {DeleteRoleModalComponent} from './delete-role-modal/delete-role-modal.component'
import {NewRoleModalComponent} from './new-role-modal/new-role-modal.component'

export {AddRoleModalComponent, DeleteRoleModalComponent, NewRoleModalComponent}
