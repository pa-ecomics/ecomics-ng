/*
 * Project : ecomics-ng
 * FileName : delete-role-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {Role} from '../../../../models';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserHttpService} from '../../../../services/http';
import {RoleSelectService} from '../../../../services/role-select/role-select.service';
import {SessionService, UsersListService} from '../../../../services';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-delete-role-modal',
    templateUrl: './delete-role-modal.component.html',
    styleUrls: ['./delete-role-modal.component.scss'],
    animations: [fadeInOut]
})
export class DeleteRoleModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    selectedRole: Role;

    constructor(private activeModal: NgbActiveModal, private userHttp: UserHttpService, private selectRoleService: RoleSelectService, private usersListService: UsersListService, private sessionService: SessionService) {
    }

    ngOnInit() {
    }

    selectRoleEvent(role: Role) {
        this.selectedRole = role;
    }

    deleteRole() {
        if (window.confirm('Delete this role ?')) {
            this.userHttp.deleteRole(this.selectedRole.id).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = 'Role deleted !';
                    this.selectRoleService.removeRole(this.selectedRole);

                    /*Remove Role for current user session*/
                    const currentUser = this.sessionService.getUser();
                    currentUser.removeRole(this.selectedRole);
                    this.sessionService.setUser(currentUser);

                    /*Remove role for all user*/
                    this.usersListService.removeRoleForAllUser(this.selectedRole);
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            });
        }
    }
}
