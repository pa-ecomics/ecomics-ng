/*
 * Project : ecomics-ng
 * FileName : add-role-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Role, User} from '../../../../models';
import {UserHttpService} from '../../../../services/http';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-add-role-modal',
    templateUrl: './add-role-modal.component.html',
    styleUrls: ['./add-role-modal.component.scss'],
    animations: [fadeInOut]
})
export class AddRoleModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    user: User;
    selectedRole: Role;

    constructor(private activeModal: NgbActiveModal, private userHttp: UserHttpService) {
    }

    ngOnInit() {
    }

    selectRoleEvent(role) {
        this.selectedRole = role;
    }

    addRole() {
        this.userHttp.addRole(this.user.id, this.selectedRole.id).subscribe((response) => {
            if (response.succeed) {
                this.errorMessage = null;
                this.successMessage = 'Role added !';
                this.user.addRole(this.selectedRole);
            } else {
                this.errorMessage = response.message;
                this.successMessage = null;
            }

            setTimeout(() => {
                this.successMessage = null;
                this.errorMessage = null;
            }, 4000);
        });
    }
}
