/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {CreateFilterModalComponent} from './create-filter-modal/create-filter-modal.component'
import {DeleteFilterModalComponent} from './delete-filter-modal/delete-filter-modal.component'

export {DeleteFilterModalComponent, CreateFilterModalComponent}
