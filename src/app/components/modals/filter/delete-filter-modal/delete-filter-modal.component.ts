/*
 * Project : ecomics-ng
 * FileName : delete-filter-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TemplateHttpService} from '../../../../services/http';
import {FilterSelectService} from '../../../../services/filter select/filter-select.service';
import {Filter} from '../../../../models';
import {TemplatePageService} from '../../../../services/template-page/template-page.service';
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-delete-filter-modal',
    templateUrl: './delete-filter-modal.component.html',
    styleUrls: ['./delete-filter-modal.component.scss'],
    animations: [fadeInOut]
})
export class DeleteFilterModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    selectedFilter: Filter;

    constructor(private activeModal: NgbActiveModal, private templateHttp: TemplateHttpService, private filterSelectService: FilterSelectService, private templatePageService: TemplatePageService) {
    }

    ngOnInit() {
    }

    selectFilterEvent(filter: Filter) {
        this.selectedFilter = filter;
    }

    deleteFilter() {
        if (window.confirm('Delete this filter ?')) {
            this.templateHttp.deleteFilter(this.selectedFilter.id).subscribe((response) => {
                if (response.succeed) {
                    this.errorMessage = null;
                    this.successMessage = 'Filter deleted !';
                    this.filterSelectService.removeFilter(this.selectedFilter);
                    this.templatePageService.removeAllFilterOnTemplates(this.selectedFilter);
                } else {
                    this.errorMessage = response.message;
                    this.successMessage = null;
                }

                setTimeout(() => {
                    this.successMessage = null;
                    this.errorMessage = null;
                }, 4000);
            });
        }
    }
}
