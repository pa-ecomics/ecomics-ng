/*
 * Project : ecomics-ng
 * FileName : delete-filter-modal.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFilterModalComponent } from './delete-filter-modal.component';

describe('DeleteFilterModalComponent', () => {
  let component: DeleteFilterModalComponent;
  let fixture: ComponentFixture<DeleteFilterModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteFilterModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFilterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
