/*
 * Project : ecomics-ng
 * FileName : create-filter-modal.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {TemplateHttpService, UserHttpService} from "../../../../services/http";
import {template} from "@babel/core";
import {FilterSelectService} from "../../../../services/filter select/filter-select.service";
import {Filter} from "../../../../models";
import {fadeInOut} from '../../../../animations';

@Component({
    selector: 'app-create-filter-modal',
    templateUrl: './create-filter-modal.component.html',
    styleUrls: ['./create-filter-modal.component.scss'],
    animations: [fadeInOut]
})
export class CreateFilterModalComponent implements OnInit {
    errorMessage: string;
    successMessage: string;
    filterLabel: string;
    filterUrl: string;

    constructor(private activeModal: NgbActiveModal, private templateHttp: TemplateHttpService, private filterSelectService: FilterSelectService) {
    }

    ngOnInit() {
    }

    createFilter() {
        this.templateHttp.createFilter(this.filterLabel, this.filterUrl).subscribe((response) => {
            if (response.succeed) {
                this.errorMessage = null;
                this.successMessage = "Filter created !";

                this.filterSelectService.filters.push(Filter.parse(response.data))
            } else {
                this.errorMessage = response.message;
                this.successMessage = null;
            }

            setTimeout(() => {
                this.successMessage = null;
                this.errorMessage = null;
            }, 4000);
        });
    }
}
