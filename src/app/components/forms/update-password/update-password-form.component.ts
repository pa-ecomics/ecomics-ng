/*
 * Project : ecomics-ng
 * FileName : update-password-form.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-update-password-form',
    templateUrl: './update-password-form.component.html',
    styleUrls: ['./update-password-form.component.scss'],
    animations: [fadeInOut]
})
export class UpdatePasswordFormComponent implements OnInit {

    constructor(private userService: UserService) {
    }

    ngOnInit() {
    }

}
