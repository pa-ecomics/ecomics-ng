/*
 * Project : ecomics-ng
 * FileName : update-user-form.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-update-user-form',
    templateUrl: './update-user-form.component.html',
    styleUrls: ['./update-user-form.component.scss'],
    animations: [fadeInOut]
})
export class UpdateUserFormComponent implements OnInit {
    constructor(private userService: UserService) {
    }

    ngOnInit() {
        this.userService.initUser();
    }
}
