/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {LoginFormComponent} from './login/login-form.component'
import {RegisterFormComponent} from './register/register-form.component'
import {UpdateUserFormComponent} from './update-user/update-user-form.component'
import {UpdatePasswordFormComponent} from './update-password/update-password-form.component'
import {NewTemplateFormComponent} from './new-template/new-template-form.component'

export {LoginFormComponent, RegisterFormComponent, UpdateUserFormComponent, UpdatePasswordFormComponent, NewTemplateFormComponent}
