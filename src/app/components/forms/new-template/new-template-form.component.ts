/*
 * Project : ecomics-ng
 * FileName : new-template-form.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {TemplateModelService} from '../../../services/templateModel/template-model.service';
import {Filter} from '../../../models';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-new-template-form',
    templateUrl: './new-template-form.component.html',
    styleUrls: ['./new-template-form.component.scss'],
    animations: [fadeInOut]
})
export class NewTemplateFormComponent implements OnInit {
    constructor(private templateModelService: TemplateModelService) {
    }

    ngOnInit() {
    }

    selectFilterEvent(filter: Filter) {
        this.templateModelService.filter = filter;
    }
}
