/*
 * Project : ecomics-ng
 * FileName : login-form.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginFormComponent} from './login-form.component';
import {NgbAlert} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {AuthService, AuthServiceConfig} from "angularx-social-login";
import {provideConfig} from "../../../modules";
import {RouterTestingModule} from "@angular/router/testing";

describe('LoginFormComponent', () => {
    let component: LoginFormComponent;
    let fixture: ComponentFixture<LoginFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginFormComponent, NgbAlert],
            imports: [FormsModule, RouterTestingModule, HttpClientModule],
            providers: [AuthService, {provide: AuthServiceConfig, useFactory: provideConfig}],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
