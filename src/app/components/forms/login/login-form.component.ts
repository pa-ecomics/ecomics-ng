/*
 * Project : ecomics-ng
 * FileName : login-form.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss'],
    animations: [fadeInOut]
})
export class LoginFormComponent implements OnInit {

    constructor(private userService: UserService) {
    }

    ngOnInit() {
    }
}
