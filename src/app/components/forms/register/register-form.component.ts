/*
 * Project : ecomics-ng
 * FileName : register-form.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {fadeInOut} from '../../../animations';

@Component({
    selector: 'app-register-form',
    templateUrl: './register-form.component.html',
    styleUrls: ['./register-form.component.scss'],
    animations: [fadeInOut]
})
export class RegisterFormComponent implements OnInit {

    constructor(private userService: UserService) {
    }

    ngOnInit() {
        this.userService.confirmPassword = null;
        this.userService.firstName = null;
        this.userService.lastName = null;
    }
}
