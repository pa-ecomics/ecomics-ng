/*
 * Project : ecomics-ng
 * FileName : page-view.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, Input, OnInit} from '@angular/core';
import Page from "../../../models/Page";
import {TemplateSettingService} from "../../../services/template-setting/template-setting.service";
import {ModalsService} from "../../../services/modals/modals.service";
import Frame from "../../../models/Frame";
import {EditBdService} from "../../../services/edit-bd/edit-bd.service";

@Component({
    selector: 'app-page-view',
    templateUrl: './page-view.component.html',
    styleUrls: ['./page-view.component.scss']
})
export class PageViewComponent implements OnInit {
    @Input() page: Page;
    @Input() isTemplate: boolean;
    service: TemplateSettingService | EditBdService;

    constructor(private templateSettingService: TemplateSettingService, private editBdService: EditBdService,  private modalsService: ModalsService) {
    }

    ngOnInit() {
        this.service = this.isTemplate ? this.templateSettingService : this.editBdService;
    }

    filterTokenFrame() {
        return this.page.frames.filter(frame => {
            return frame.isTaken;
        });
    }

    filterFrames() {
        if (this.isTemplate) {
            return this.page.frames;
        }

        return this.page.frames.filter(frame => {
            return !frame.isTaken;
        });
    }

    updateFrame(frame: Frame) {
        this.modalsService.openModalUpdateFrame(frame, this.isTemplate);
    }
}
