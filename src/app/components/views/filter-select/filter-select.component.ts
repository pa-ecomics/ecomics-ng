/*
 * Project : ecomics-ng
 * FileName : filter-select.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FilterSelectService} from "../../../services/filter select/filter-select.service";
import {Filter} from "../../../models";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-filter-select',
    templateUrl: './filter-select.component.html',
    styleUrls: ['./filter-select.component.scss']
})
export class FilterSelectComponent implements OnInit {
    @Output() filterSelected = new EventEmitter<Filter>();
    selectFilterList: any;

    constructor(private filterSelectService: FilterSelectService) {
    }

    ngOnInit() {
    }

    handleChange(selectedItem) {
        this.filterSelected.emit(selectedItem);
    }

}
