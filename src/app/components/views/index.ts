/*
 * Project : ecomics-ng
 * FileName : index.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {TemplateViewComponent} from './template-view/template-view.component'
import {TagSelectComponent} from './tag-select/tag-select.component'
import {UsersListTableComponent} from './users-list-table/users-list-table.component'
import {RoleSelectComponent} from './role-select/role-select.component'
import {PageViewComponent} from './page-view/page-view.component'
import {BdViewComponent} from './bd-view/bd-view.component'
import {FilterSelectComponent} from './filter-select/filter-select.component'
import {DownloadApkComponent} from './download-apk/download-apk.component'

export {TemplateViewComponent, TagSelectComponent, UsersListTableComponent, RoleSelectComponent, PageViewComponent, BdViewComponent, FilterSelectComponent, DownloadApkComponent}
