/*
 * Project : ecomics-ng
 * FileName : role-select.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Role} from "../../../models";
import {RoleSelectService} from "../../../services/role-select/role-select.service";

@Component({
    selector: 'app-role-select',
    templateUrl: './role-select.component.html',
    styleUrls: ['./role-select.component.scss']
})
export class RoleSelectComponent implements OnInit {
    @Output() roleSelected = new EventEmitter<Role>();
    selectRoleList: any;

    constructor(private roleSelectService: RoleSelectService) {
    }

    ngOnInit() {
    }

    handleChange(selectedItem) {
        this.roleSelected.emit(selectedItem);
    }
}
