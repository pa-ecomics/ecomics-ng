/*
 * Project : ecomics-ng
 * FileName : bd-view.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, Input, OnInit} from '@angular/core';
import Bd from "../../../models/Bd";
import {BdHttpService} from "../../../services/http";
import {HomePageService} from "../../../services/home-page/home-page.service";
import {Url} from "../../../enums";
import {EditBdService} from "../../../services/edit-bd/edit-bd.service";
import {RouterService} from "../../../services/router/router.service";
import {HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";


@Component({
    selector: 'app-bd-view',
    templateUrl: './bd-view.component.html',
    styleUrls: ['./bd-view.component.scss']
})
export class BdViewComponent implements OnInit {
    @Input() bd: Bd;

    constructor(private homePageService: HomePageService, private bdHttp: BdHttpService, private editBdService: EditBdService, private routerService: RouterService, private httpClient: HttpClient) {
    }

    ngOnInit() {
    }

    deleteBd() {
        if (window.confirm('Delete this bd ?')) {
            this.bdHttp.deleteBd(this.bd.id).subscribe((response) => {
                if (response.succeed) {
                    this.homePageService.successMessage = 'Bd deleted !';
                    this.homePageService.errorMessage = null;
                    this.homePageService.removeBd(this.bd);
                } else {
                    this.homePageService.successMessage = null;
                    this.homePageService.errorMessage = response.message;
                }

                setTimeout(() => {
                    this.homePageService.successMessage = null;
                    this.homePageService.errorMessage = null;
                }, 4000);
            });
        }
    }

    editMyBd() {
        this.editBdService.initView(this.bd);
        this.routerService.navigate(Url.editBd);
    }

    downloadBd() {
        if (window.confirm('download this bd ?')) {
            return this.bdHttp.downloadBd(this.bd.id).subscribe((response) => {
                if (response.succeed) {
                    let aLink = document.createElement("a");
                    aLink.setAttribute('style', 'display:none;');
                    document.body.appendChild(aLink);
                    aLink.href = `${environment.apiUrl}/api/shared/bd/${this.bd.id}/download.pdf`;
                    aLink.download = this.bd.template.title.replace(' ', '_') + '.pdf';
                    aLink.target = '_blank';
                    aLink.click();
                }
            });
        }
    }
}
