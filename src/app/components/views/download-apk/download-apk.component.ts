import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-download-apk',
    templateUrl: './download-apk.component.html',
    styleUrls: ['./download-apk.component.scss']
})
export class DownloadApkComponent implements OnInit {
    @Input() textLink: String;

    constructor() {
    }

    ngOnInit() {
    }

}
