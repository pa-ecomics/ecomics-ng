/*
 * Project : ecomics-ng
 * FileName : users-list-table.component.spec.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersListTableComponent } from './users-list-table.component';

describe('UsersListTableComponent', () => {
  let component: UsersListTableComponent;
  let fixture: ComponentFixture<UsersListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
