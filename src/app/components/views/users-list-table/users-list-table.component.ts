/*
 * Project : ecomics-ng
 * FileName : users-list-table.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit} from '@angular/core';
import {SessionService, UsersListService} from "../../../services";
import {ModalsService} from "../../../services/modals/modals.service";

@Component({
    selector: 'app-users-list-table',
    templateUrl: './users-list-table.component.html',
    styleUrls: ['./users-list-table.component.scss']
})
export class UsersListTableComponent implements OnInit {

    constructor(private userListService: UsersListService, private sessionService: SessionService, private modalsService: ModalsService) {
    }

    ngOnInit() {
    }
}
