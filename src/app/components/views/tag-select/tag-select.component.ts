/*
 * Project : ecomics-ng
 * FileName : tag-select.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {Tag} from "../../../models";
import {TagSelectService} from "../../../services/tag-select/tag-select.service";

@Component({
    selector: 'app-tag-select',
    templateUrl: './tag-select.component.html',
    styleUrls: ['./tag-select.component.scss']
})
export class TagSelectComponent implements OnInit {
    @Input() showAll: boolean;
    @Output() tagSelected = new EventEmitter<Tag>();
    selectTagList: any;

    constructor(private tagSelectService: TagSelectService) {
    }

    ngOnInit() {
        this.selectTagList = null;
    }

    handleChange(selectedItem) {
        this.tagSelected.emit(selectedItem);
    }
}
