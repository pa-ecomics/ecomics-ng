/*
 * Project : ecomics-ng
 * FileName : template-view.component.ts
 * Copyright (c) created by greg at 13.06.19
 * Description :
 */

import {Component, Input, OnInit} from '@angular/core';
import {Tag, Template} from "../../../models";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SessionService} from "../../../services";
import {BdHttpService, TemplateHttpService} from "../../../services/http";
import {ModalsService} from "../../../services/modals/modals.service";
import {TemplatePageService} from "../../../services/template-page/template-page.service";
import {Url} from "../../../enums";
import {RouterService} from "../../../services/router/router.service";
import {TemplateSettingService} from "../../../services/template-setting/template-setting.service";

@Component({
    selector: 'app-template-view',
    templateUrl: './template-view.component.html',
    styleUrls: ['./template-view.component.scss']
})
export class TemplateViewComponent implements OnInit {
    @Input() template: Template;
    Url = Url;

    constructor(private modalService: NgbModal, private sessionService: SessionService, private templateHttp: TemplateHttpService, private bdHttp: BdHttpService, private modalsService: ModalsService, private templateService: TemplatePageService, private routerService: RouterService, private templateSettingService: TemplateSettingService) {
    }

    ngOnInit() {
    }

    removeTag(tag: Tag) {
        if (window.confirm('Delete this tag ?')) {
            this.templateHttp.removeTag(this.template.id, tag.id).subscribe((response) => {
                if (response.succeed) {
                    this.templateService.successMessage = 'Tag deleted !';
                    this.templateService.errorMessage = null;
                    this.template.removeTag(tag);
                } else {
                    this.templateService.successMessage = null;
                    this.templateService.errorMessage = response.message;
                }

                setTimeout(() => {
                    this.templateService.successMessage = null;
                    this.templateService.errorMessage = null;
                }, 4000);
            });
        }
    }

    deleteTemplate() {
        if (window.confirm('Delete this template ?')) {
            this.templateHttp.deleteTemplate(this.template.id).subscribe((response) => {
                if (response.succeed) {
                    this.templateService.successMessage = 'Template deleted !';
                    this.templateService.errorMessage = null;
                    this.templateService.removeTemplate(this.template);
                } else {
                    this.templateService.successMessage = null;
                    this.templateService.errorMessage = response.message;
                }

                setTimeout(() => {
                    this.templateService.successMessage = null;
                    this.templateService.errorMessage = null;
                }, 4000);
            });
        }
    }

    goToSettingTemplatePage() {
        this.templateSettingService.initView(this.template);
        this.routerService.navigate(Url.templateSetting);
    }

    createUserBdFromTemplate() {
        this.bdHttp.createBd(this.template.id).subscribe((response) => {
            if (response.succeed) {
                this.routerService.navigate(Url.home);
            } else {
                this.templateService.successMessage = null;
                this.templateService.errorMessage = response.message;
            }
        });
    }
}
