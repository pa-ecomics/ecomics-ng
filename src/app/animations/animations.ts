import {animate, state, style, transition, trigger} from '@angular/animations';

const fadeInOut = trigger('fadeInOut', [
    state('void', style({
        opacity: 0
    })),
    transition('void <=> *', animate(1000)),
]);

export {fadeInOut};
