export const environment = {
  production: true,
  apiUrl: 'https://picture-factory.site/',
  tokenErrorMessage: ['invalid signature', 'token malformed', 'session expired'],
  cryptoKey: 'fac2019Pict',
  googleClientId: '799790593306-pfj4n51jnkcnf3ebosp5c981ulq15i7s.apps.googleusercontent.com',
  facebookClientId: '691304337939566'
};
