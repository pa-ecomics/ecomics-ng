// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    // apiUrl: 'http://localhost:3001',
    // sharedDirectory: 'http://localhost:3001/api/shared',
    apiUrl: 'https://picture-factory.site/api',
    sharedDirectory: '/Shared',
    tokenErrorMessage: ['invalid signature', 'token malformed', 'session expired'],
    cryptoKey: 'fac2019Pict',
    googleClientId: '799790593306-pfj4n51jnkcnf3ebosp5c981ulq15i7s.apps.googleusercontent.com',
    facebookClientId: '691304337939566'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
