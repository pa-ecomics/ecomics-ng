From node:11.12.0 as builder

WORKDIR /App
COPY package.json /App/
COPY yarn.lock /App/
RUN yarn install
COPY ./ /App/
RUN yarn build

#FROM nginx:1.15
FROM nginx:1.14.1-alpine
COPY ./nginx.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /App/dist/ecomics-ng/ /var/www/html

EXPOSE 3005
CMD ["nginx", "-g", "daemon off;"]
